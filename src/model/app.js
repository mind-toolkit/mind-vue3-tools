import {reactive} from 'vue'

const state = reactive({
	isCollapse: false,
  nav: false
})

export default state
