//Importar helpers
import fetch from './helper/fetch'
import Model from './helper/model'

export {
	fetch,
	Model
}

// Importar Element Plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

//Importar componentes
import mindApp from './components/mind-app.vue'
import mindView from './components/mind-view.vue'
import mindTable from './components/mind-table.vue'
import mindForm from './components/mind-form.vue'
import mindFilter from './components/mind-filter.vue'
import mindDashboard from './components/mind-dashboard.vue'
import mindWidgetContainer from './components/mind-widget-container.vue'
import mindWidgetIndicatorText from './components/widgets/indicator-text.vue'
import mindWidgetChart from './components/widgets/chart.vue'
// import mindReport from './components/mind-report.vue'
// import chartJs from './components/widgets/chartjs'



export default (app) => {
  registerElermentPlus(app)
  registerComponents(app)


}


function registerElermentPlus(app) {
	// Registro Element Plus
	app.use(ElementPlus)
	for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
		app.component(key, component)
	}

}

function registerComponents(app) {
	// Registro los componentes
	app.component('mind-app', mindApp)
	app.component('mind-view', mindView)
	app.component('mind-table', mindTable)
	app.component('mind-form', mindForm)
	app.component('mind-filter', mindFilter)
	app.component('mind-dashboard', mindDashboard)
	app.component('mind-widget-container', mindWidgetContainer)
	app.component('mind-widget-indicator-text', mindWidgetIndicatorText)
	app.component('mind-widget-chart', mindWidgetChart)
	// app.component('mind-report', mindReport)
	// app.component('chartJs', chartJs)

}