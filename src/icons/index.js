import iconPdf from './pdf.vue'
import iconXls from './xls.vue'

export {
	iconPdf,
	iconXls
}

